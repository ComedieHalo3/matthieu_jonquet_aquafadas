/**
 * Created by matth on 05/05/2017.
 */
app.controller('mainCtrl', ['$scope', '$location',
    function($scope, $location){
        $scope.displaySidebar = false;

        $scope.isMenuActive = function(url, strict){
            if(!strict) {
                return $location.path().match(url);
            } else {
                return $location.path() === url;
            }
        };
    }
]);