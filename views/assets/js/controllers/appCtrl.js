/**
 * Created by matth on 05/05/2017.
 */
app.controller('appCtrl', ['$scope', 'HomeService', '$cookieStore',
    function($scope, HomeService, $cookieStore){
        $scope.apiKey = '76e8e8d2283c6d0bb1351dd32dc6229b';
        $scope.idCity = 2992166;

        $scope.cityName = $cookieStore.get('cityName');
        $scope.cityTemperature = $cookieStore.get('cityTemperature');
        $scope.citySky = $cookieStore.get('citySky');


        $scope.cityWeather = function(){
            HomeService.getCity($scope.idCity, $scope.apiKey ,function(data){
                $scope.city = data.name;
                $scope.temperature = (data.main.temp).toFixed(0);
                $scope.sky = data.weather[0].icon;
            });
            if($scope.cityName == null){
                $cookieStore.put('cityName', true);
                $scope.cityName = $cookieStore.get('cityName');
            }
            if($scope.cityTemperature == null){
                $cookieStore.put('cityTemperature', true);
                $scope.cityTemperature = $cookieStore.get('cityTemperature');
            }
            if($scope.citySky == null){
                $cookieStore.put('citySky', true);
                $scope.citySky = $cookieStore.get('citySky');
            }
        };

        $scope.setting = function(){
            $scope.changeCity = function(){
                if($scope.cityName == true){
                    $cookieStore.put('cityName', false);
                } else {
                    $cookieStore.put('cityName', true);
                }
            };

            $scope.changeTemperature = function(){
                if($scope.cityTemperature == true){
                    $cookieStore.put('cityTemperature', false);
                } else {
                    $cookieStore.put('cityTemperature', true);
                }
            };

            $scope.changeSky = function(){
                if($scope.citySky == true){
                    $cookieStore.put('citySky', false);
                } else {
                    $cookieStore.put('citySky', true);
                }
            };
        };
    }
]);
