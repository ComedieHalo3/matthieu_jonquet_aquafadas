/**
 * Created by matth on 05/05/2017.
 */
app.factory('HomeService', ['$http',
    function($http){
        return {
            getCity : function(id, apiKey, callback) {
                $http({
                    method: 'GET',
                    url: 'http://api.openweathermap.org/data/2.5/weather?units=Metric&id=' + id + '&appid=' + apiKey
                })
                    .success(function(data){
                        (callback || angular.noop)(data)
                    });

            }
        }
    }
]);
