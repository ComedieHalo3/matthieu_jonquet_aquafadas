/**
 * Created by matth on 05/05/2017.
 */
var app = angular.module('weather', ['ngRoute', 'ngCookies'])
    .config(['$routeProvider',
        function($routeProvider){
            $routeProvider
            .when('/home', {
                templateUrl: 'matthieu_jonquet_aquafadas/views/templates/home/home.html',
                controller: 'appCtrl'
            })
            .when('/setting', {
                templateUrl: 'matthieu_jonquet_aquafadas/views/templates/setting/setting.html',
                controller: 'appCtrl'
            })
            .otherwise({
                redirectTo: '/home'
            });
        }
    ]);
