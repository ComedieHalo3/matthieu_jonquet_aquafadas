Installation de l'application :

Pour windows :

- Installer wamp sur votre machine (http://www.wampserver.com/).

- Dans les dossiers wamp o� vous l'avez install�, mettre le site dans le dossier www.

- Lancer l'application wamp.

- Sur votre navigateur, Aller sur l'adresse : http://localhost/matthieu_jonquet_aquafadas/views/templates

Pour Mac :

- Installer mamp sur votre machine (https://www.mamp.info).

- Dans les dossiers de mamp, mettre le site dans le dossier htdocs.

- Lancer l'application mamp.

- Sur votre navigateur, Aller sur l'adresse : http://localhost/matthieu_jonquet_aquafadas/views/templates

Pour Linux :

- Installer lamp sur votre machine (https://doc.ubuntu-fr.org/lamp).

- Aller dans /var/www/html/, mettre le site dans ce dossier.

- Sur votre navigateur, Aller sur l'adresse : http://localhost/matthieu_jonquet_aquafadas/views/templates